package utils;

import com.inditex.itselection.application.domain.Brand;
import com.inditex.itselection.repository.models.BrandMO;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BrandMocks {
    public Brand createMockBrand(Integer id, String name) {
        Brand brand = new Brand();
        brand.setId(id);
        brand.setName(name);
        return brand;
    }

    public BrandMO createMockBrandMO(Integer id, String name) {
        BrandMO brand = new BrandMO();
        brand.setId(id);
        brand.setName(name);
        return brand;
    }
}
