package utils;

import com.inditex.itselection.application.domain.Brand;
import com.inditex.itselection.application.domain.Price;
import com.inditex.itselection.repository.models.BrandMO;
import com.inditex.itselection.repository.models.PriceMO;
import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;

@UtilityClass
public class PriceMocks {
    public Price createMockPrice(Integer id, Integer brandId, Integer productId, LocalDateTime startDate,
                                 LocalDateTime endDate, Integer priceList, Integer priority, Double price,
                                 String currency, Brand brand) {
        Price priceMock = new Price();
        priceMock.setId(id);
        priceMock.setBrand(brand);
        priceMock.setStartDate(startDate);
        priceMock.setEndDate(endDate);
        priceMock.setPriceList(priceList);
        priceMock.setProductId(productId);
        priceMock.setPriority(priority);
        priceMock.setPrice(price);
        priceMock.setCurrency(currency);
        return priceMock;
    }

    public PriceMO createMockPriceMO(Integer id, Integer brandId, Integer productId, LocalDateTime startDate,
                                     LocalDateTime endDate, Integer priceList, Integer priority, Double price,
                                     String currency, BrandMO brandMO) {
        PriceMO priceMock = new PriceMO();
        priceMock.setId(id);
        priceMock.setBrand(brandMO);
        priceMock.setStartDate(startDate);
        priceMock.setEndDate(endDate);
        priceMock.setPriceList(priceList);
        priceMock.setProductId(productId);
        priceMock.setPriority(priority);
        priceMock.setPrice(price);
        priceMock.setCurrency(currency);
        return priceMock;
    }
}
