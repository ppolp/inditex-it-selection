package integration.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestConfigConstants {
    public static final String BASE_JSON_FOLDER = "/json";

    public static final String PRICES_JSON = BASE_JSON_FOLDER + "/prices";
}
