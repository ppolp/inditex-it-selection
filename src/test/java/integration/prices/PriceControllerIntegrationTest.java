package integration.prices;

import com.inditex.itselection.ItselectionApplication;
import integration.BaseMvcTest;
import integration.common.TestConfigConstants;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ItselectionApplication.class)
@AutoConfigureMockMvc
public class PriceControllerIntegrationTest extends BaseMvcTest {
    @Autowired
    protected MockMvc mvc;

    @Test
    @WithMockUser(username = "testUser", roles = "USER")
    void getPrices1_then200() throws Exception {
        this.mvc.perform(get("/prices")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("date", "2020-06-14T10:00:00")
                        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(jsonResourceAsString(TestConfigConstants.PRICES_JSON + "/test-1-prices" +
                        ".json")));
    }

    @Test
    @WithMockUser(username = "testUser", roles = "USER")
    void getPrices2_then200() throws Exception {
        this.mvc.perform(get("/prices")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("date", "2020-06-14T16:00:00")
                        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(jsonResourceAsString(TestConfigConstants.PRICES_JSON + "/test-2-prices" +
                        ".json")));
    }

    @Test
    @WithMockUser(username = "testUser", roles = "USER")
    void getPrices3_then200() throws Exception {
        this.mvc.perform(get("/prices")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("date", "2020-06-14T21:00:00")
                        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(jsonResourceAsString(TestConfigConstants.PRICES_JSON + "/test-3-prices" +
                        ".json")));
    }

    @Test
    @WithMockUser(username = "testUser", roles = "USER")
    void getPrices4_then200() throws Exception {
        this.mvc.perform(get("/prices")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("date", "2020-06-15T10:00:00")
                        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(jsonResourceAsString(TestConfigConstants.PRICES_JSON + "/test-4-prices" +
                        ".json")));
    }

    @Test
    @WithMockUser(username = "testUser", roles = "USER")
    void getPrices5_then200() throws Exception {
        this.mvc.perform(get("/prices")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("date", "2020-06-16T21:00:00")
                        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().json(jsonResourceAsString(TestConfigConstants.PRICES_JSON + "/test-5-prices" +
                        ".json")));
    }
}
