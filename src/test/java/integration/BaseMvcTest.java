package integration;

import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public abstract class BaseMvcTest {
    protected String jsonResourceAsString(String file) throws IOException {
        String result;

        try (InputStream resource = new ClassPathResource(file).getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
            result = reader.lines().collect(Collectors.joining("\n"));
        }

        return result;
    }
}
