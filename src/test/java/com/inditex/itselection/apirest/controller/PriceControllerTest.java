package com.inditex.itselection.apirest.controller;

import com.inditex.itselection.application.domain.Brand;
import com.inditex.itselection.application.domain.Price;
import com.inditex.itselection.application.ports.in.price.DeletePriceByIdServicePort;
import com.inditex.itselection.application.ports.in.price.GetPriceByBrandProductDateServicePort;
import com.inditex.itselection.application.ports.in.price.GetPriceByIdServicePort;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import utils.PriceMocks;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PriceControllerTest {

    @InjectMocks
    private PriceController priceController;

    @Mock
    private GetPriceByIdServicePort getPriceByIdServicePort;

    @Mock
    private GetPriceByBrandProductDateServicePort getPriceByBrandProductDateServicePort;

    @Mock
    private DeletePriceByIdServicePort deletePriceByIdServicePort;

    private Brand createMockBrand() {
        Brand brand = new Brand();
        brand.setId(1);
        brand.setName("Zara");
        return brand;
    }

    @Nested
    class getPriceByParams {
        @Test
        void when_priceIsAvailable_expect_200() {
            var date = LocalDateTime.of(2020, 1, 24, 11, 30);
            var productId = 1;
            var brandId = 1;
            var mockPrice = PriceMocks.createMockPrice(
                    1,
                    createMockBrand().getId(),
                    1,
                    LocalDateTime.of(2020, 8, 31, 11, 30),
                    LocalDateTime.of(2024, 1, 24, 11, 30),
                    1,
                    1,
                    1.0,
                    "EUR",
                    createMockBrand());

            when(getPriceByBrandProductDateServicePort.execute(brandId, productId, date))
                    .thenReturn(mockPrice);

            var result = priceController.getPriceByParams(date, productId, brandId);
            assertNotNull(result);
            assertEquals(productId, result.getBody().getProductId());
            assertEquals(brandId, result.getBody().getBrandId());
        }
    }

    @Nested
    class getPriceById {
        @Test
        void when_priceIsAvailable_expect_200() {
            when(getPriceByIdServicePort.execute(anyInt())).thenReturn(new Price());
            assertNotNull(priceController.getPriceById(1));
        }
    }

    @Nested
    class deletePrice {
        @Test
        void deletePrice() {
            deletePriceByIdServicePort.execute(anyInt());
            assertNotNull(priceController.deletePrice(1));
        }
    }
}