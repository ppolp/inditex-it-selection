package com.inditex.itselection.application.services.price;

import com.inditex.itselection.application.ports.out.PriceRepositoryPort;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import utils.BrandMocks;
import utils.PriceMocks;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetPriceByBrandProductDateServiceUseCaseTest {

    @Mock
    private PriceRepositoryPort priceRepositoryPort;

    @InjectMocks
    private GetPriceByBrandProductDateServiceUseCase getPriceByBrandProductDateServiceUseCase;

    @Nested
    class execute {
        public static final int BRAND_ID = 1;
        public static final int PRODUCT_ID = 35455;

        public static final LocalDateTime DATE = LocalDateTime.of(2020, 6, 14, 10, 0);

        @Test
        void when_priceIsAvailable_expect_price() {
            var brand = BrandMocks.createMockBrand(BRAND_ID, "ZARA");
            var price = PriceMocks.createMockPrice(1, BRAND_ID, PRODUCT_ID, LocalDateTime.of(2020, 6, 14, 0, 0),
                    LocalDateTime.of(2020, 12, 31, 23, 59), 1, 0, 35.5, "EUR", brand);

            when(priceRepositoryPort.findPriceByBrandAndProductAndDate(BRAND_ID, PRODUCT_ID, DATE)).thenReturn(price);

            var result = getPriceByBrandProductDateServiceUseCase.execute(BRAND_ID, PRODUCT_ID, DATE);
            assertNotNull(result);
            assertEquals(BRAND_ID, result.getBrand().getId());
            assertEquals(PRODUCT_ID, result.getProductId());
        }
    }
}