package com.inditex.itselection.repository.adapters;

import com.inditex.itselection.repository.PriceMOJpaRepository;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import utils.BrandMocks;
import utils.PriceMocks;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PriceRepositoryAdapterTest {

    public static final int BRAND_ID = 1;
    public static final LocalDateTime DATE = LocalDateTime.of(2020, BRAND_ID, 24, 11, 30);
    public static final int PRODUCT_ID = 1;
    @Mock
    private PriceMOJpaRepository repository;

    @InjectMocks
    private PriceRepositoryAdapter priceRepositoryAdapter;

    @Nested
    class findPriceByBrandAndProductAndDate {
        @Test
        void when_priceIsAvailable_expect_price() {
            var brand = BrandMocks.createMockBrandMO(BRAND_ID, "ZARA");
            var price = PriceMocks.createMockPriceMO(1, BRAND_ID, PRODUCT_ID,
                    LocalDateTime.of(2020, 6, 14, 0, 0),
                    LocalDateTime.of(2020, 12, 31, 23, 59),
                    1, 0, 35.5, "EUR", brand);

            when(repository.findPriceByBrandAndProductAndDate(BRAND_ID, PRODUCT_ID, DATE)).thenReturn(List.of(price));

            var result = priceRepositoryAdapter
                    .findPriceByBrandAndProductAndDate(BRAND_ID, PRODUCT_ID, DATE);
            assertNotNull(result);
            assertEquals(BRAND_ID, result.getBrand().getId());
            assertEquals(PRODUCT_ID, result.getProductId());
        }

        @Test
        void when_priceIsNotAvailable_expect_NoSuchElementException() {
            when(repository.findPriceByBrandAndProductAndDate(BRAND_ID, PRODUCT_ID, DATE)).thenReturn(List.of());

            assertThrows(NoSuchElementException.class, () -> priceRepositoryAdapter
                    .findPriceByBrandAndProductAndDate(BRAND_ID, PRODUCT_ID, DATE));
        }
    }
}