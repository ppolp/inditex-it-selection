CREATE TABLE IF NOT EXISTS brands (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(100)
);

CREATE TABLE IF NOT EXISTS prices (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    brand_id int,
    start_date datetime,
    end_date datetime,
    price_list int,
    product_id int,
    priority int,
    price float,
    currency varchar(5),

    FOREIGN KEY (brand_id) REFERENCES brands(id)
);

CREATE TABLE IF NOT EXISTS users (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username varchar(50),
    password varchar(500)
);

-- Brands data
INSERT INTO brands (name) VALUES ('Zara');
INSERT INTO brands (name) VALUES ('Stradivarius');
INSERT INTO brands (name) VALUES ('Lefties');
INSERT INTO brands (name) VALUES ('Bershka');

-- Prices data
INSERT INTO prices (brand_id, start_date, end_date, price_list, product_id, priority, price, currency)
VALUES
(1, '2020-06-14 00.00.00', '2020-12-31 23.59.59', 1, 35455, 0, 35.50, 'EUR'),
(1, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 2, 35455, 1, 25.45, 'EUR'),
(1, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 3, 35455, 1, 30.50, 'EUR'),
(1, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 4, 35455, 1, 38.95, 'EUR');

-- Usuarios data
INSERT INTO users (username, password) VALUES ('inditex',
'$2a$10$DLK2UDk5j/4oE.pJAurzAeC.iugaarH.PYGy3uFrfXnTbFyKBDbYO');