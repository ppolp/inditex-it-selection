package com.inditex.itselection.repository;

import com.inditex.itselection.repository.models.BrandMO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandMOJpaRepository extends CrudRepository<BrandMO, Integer> {
}
