package com.inditex.itselection.repository;

import com.inditex.itselection.repository.models.PriceMO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PriceMOJpaRepository extends CrudRepository<PriceMO, Integer> {
    @Query("SELECT p FROM PriceMO p WHERE p.brand.id = :brandId AND p.productId = :productId AND :date >= p.startDate" +
            " AND :date <= p.endDate ORDER BY p.priority DESC")
    List<PriceMO> findPriceByBrandAndProductAndDate(@Param("brandId") Integer brandId,
                                                    @Param("productId") Integer productId,
                                                    @Param("date") LocalDateTime date);
}
