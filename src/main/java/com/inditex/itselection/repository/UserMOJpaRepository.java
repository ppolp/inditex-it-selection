package com.inditex.itselection.repository;

import com.inditex.itselection.repository.models.UserMO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserMOJpaRepository extends JpaRepository<UserMO, Integer> {
    Optional<UserMO> findByUsername(String username);
}
