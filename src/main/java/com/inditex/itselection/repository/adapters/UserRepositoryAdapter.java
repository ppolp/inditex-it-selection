package com.inditex.itselection.repository.adapters;

import com.inditex.itselection.application.domain.User;
import com.inditex.itselection.application.exceptions.InditexException;
import com.inditex.itselection.application.mappers.UserMapper;
import com.inditex.itselection.application.ports.out.UserRepositoryPort;
import com.inditex.itselection.repository.UserMOJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRepositoryAdapter implements UserRepositoryPort {

    private static final UserMapper userMapper = UserMapper.INSTANCE;
    @Autowired
    private UserMOJpaRepository userMOJpaRepository;

    @Override
    public User findByUsername(String username) {
        return userMOJpaRepository.findByUsername(username).map(userMapper::fromModel)
                .orElseThrow(() -> new InditexException("Usuario no encontrado"));
    }
}
