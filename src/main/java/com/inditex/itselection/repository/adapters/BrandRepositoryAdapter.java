package com.inditex.itselection.repository.adapters;

import com.inditex.itselection.application.domain.Brand;
import com.inditex.itselection.application.mappers.BrandMapper;
import com.inditex.itselection.application.ports.out.BrandRepositoryPort;
import com.inditex.itselection.repository.BrandMOJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class BrandRepositoryAdapter implements BrandRepositoryPort {

    private static final BrandMapper brandMapper = BrandMapper.INSTANCE;

    private final BrandMOJpaRepository repository;

    @Override
    public Brand getById(Integer id) {
        return repository.findById(id).map(brandMapper::fromModel)
                .orElseThrow(() -> new NoSuchElementException(Brand.class.getSimpleName()));
    }

    @Override
    public Brand save(Brand price) {
        return brandMapper.fromModel(repository.save(brandMapper.toModel(price)));
    }

    @Override
    public void delete(Integer id) {
        var brand = repository.findById(id).orElseThrow(() -> new NoSuchElementException(Brand.class.getSimpleName()));
        repository.deleteById(brand.getId());
    }
}
