package com.inditex.itselection.repository.adapters;

import com.inditex.itselection.application.domain.Price;
import com.inditex.itselection.application.mappers.PriceMapper;
import com.inditex.itselection.application.ports.out.PriceRepositoryPort;
import com.inditex.itselection.repository.PriceMOJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class PriceRepositoryAdapter implements PriceRepositoryPort {
    private static final PriceMapper priceMapper = PriceMapper.INSTANCE;

    private final PriceMOJpaRepository repository;

    @Override
    public Price findPriceByBrandAndProductAndDate(Integer brandId, Integer productId, LocalDateTime date) {
        return repository.findPriceByBrandAndProductAndDate(brandId, productId, date)
                .stream().findFirst().map(priceMapper::fromModel)
                .orElseThrow(() -> new NoSuchElementException(Price.class.getSimpleName()));
    }

    @Override
    public Price getById(Integer id) {
        return repository.findById(id).map(priceMapper::fromModel)
                .orElseThrow(() -> new NoSuchElementException(Price.class.getSimpleName()));
    }

    @Override
    public Price save(Price price) {
        return priceMapper.fromModel(repository.save(priceMapper.toModel(price)));
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
