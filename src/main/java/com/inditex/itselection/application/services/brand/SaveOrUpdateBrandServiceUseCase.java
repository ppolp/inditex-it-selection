package com.inditex.itselection.application.services.brand;

import com.inditex.itselection.application.domain.Brand;
import com.inditex.itselection.application.ports.in.brand.SaveOrUpdateBrandServicePort;
import com.inditex.itselection.application.ports.out.BrandRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SaveOrUpdateBrandServiceUseCase implements SaveOrUpdateBrandServicePort {

    private final BrandRepositoryPort brandRepositoryPort;

    @Override
    public Brand execute(Integer id, Brand brand) {
        if (id != null) {
            brand.setId(id);
        }
        return brandRepositoryPort.save(brand);
    }
}
