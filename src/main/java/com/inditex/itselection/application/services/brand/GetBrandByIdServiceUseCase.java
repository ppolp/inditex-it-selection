package com.inditex.itselection.application.services.brand;

import com.inditex.itselection.application.domain.Brand;
import com.inditex.itselection.application.ports.in.brand.GetBrandByIdServicePort;
import com.inditex.itselection.application.ports.out.BrandRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetBrandByIdServiceUseCase implements GetBrandByIdServicePort {

    private final BrandRepositoryPort brandRepositoryPort;

    @Override
    @Cacheable(value = "brandById")
    public Brand execute(Integer id) {
        return brandRepositoryPort.getById(id);
    }
}
