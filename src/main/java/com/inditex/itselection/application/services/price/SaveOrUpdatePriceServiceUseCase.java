package com.inditex.itselection.application.services.price;

import com.inditex.itselection.application.domain.Price;
import com.inditex.itselection.application.ports.in.price.SaveOrUpdatePriceServicePort;
import com.inditex.itselection.application.ports.out.PriceRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SaveOrUpdatePriceServiceUseCase implements SaveOrUpdatePriceServicePort {

    private final PriceRepositoryPort priceRepositoryPort;

    @Override
    public Price execute(Integer id, Price price) {
        if (id != null) {
            price.setId(id);
        }
        return priceRepositoryPort.save(price);
    }
}
