package com.inditex.itselection.application.services.price;

import com.inditex.itselection.application.domain.Price;
import com.inditex.itselection.application.ports.in.price.GetPriceByBrandProductDateServicePort;
import com.inditex.itselection.application.ports.out.PriceRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class GetPriceByBrandProductDateServiceUseCase implements GetPriceByBrandProductDateServicePort {

    private final PriceRepositoryPort priceRepositoryPort;

    @Override
    public Price execute(Integer brand, Integer productId, LocalDateTime date) {
        return priceRepositoryPort.findPriceByBrandAndProductAndDate(brand, productId, date);
    }
}
