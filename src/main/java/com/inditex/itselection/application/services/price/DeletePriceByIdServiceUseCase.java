package com.inditex.itselection.application.services.price;

import com.inditex.itselection.application.ports.in.price.DeletePriceByIdServicePort;
import com.inditex.itselection.application.ports.out.PriceRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeletePriceByIdServiceUseCase implements DeletePriceByIdServicePort {

    private final PriceRepositoryPort priceRepositoryPort;

    @Override
    public void execute(Integer id) {
        priceRepositoryPort.delete(id);
    }
}
