package com.inditex.itselection.application.services.price;

import com.inditex.itselection.application.domain.Price;
import com.inditex.itselection.application.ports.in.price.GetPriceByIdServicePort;
import com.inditex.itselection.application.ports.out.PriceRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetPriceByIdServiceUseCase implements GetPriceByIdServicePort {

    private final PriceRepositoryPort priceRepositoryPort;

    @Override
    @Cacheable(value = "priceById")
    public Price execute(Integer id) {
        return priceRepositoryPort.getById(id);
    }
}
