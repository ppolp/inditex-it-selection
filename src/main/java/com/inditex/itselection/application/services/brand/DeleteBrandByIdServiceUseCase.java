package com.inditex.itselection.application.services.brand;

import com.inditex.itselection.application.ports.in.brand.DeleteBrandByIdServicePort;
import com.inditex.itselection.application.ports.out.BrandRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteBrandByIdServiceUseCase implements DeleteBrandByIdServicePort {

    private final BrandRepositoryPort brandRepositoryPort;

    @Override
    public void execute(Integer id) {
        brandRepositoryPort.delete(id);
    }
}
