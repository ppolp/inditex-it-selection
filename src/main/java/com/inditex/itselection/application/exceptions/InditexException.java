package com.inditex.itselection.application.exceptions;

import java.io.Serial;

public class InditexException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 616007099932873358L;

    public InditexException(String message) {
        super(message);
    }
}
