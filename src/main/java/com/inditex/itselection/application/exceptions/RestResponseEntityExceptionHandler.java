package com.inditex.itselection.application.exceptions;

import com.inditex.itselection.apirest.dto.ApiErrorDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {NoSuchElementException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        var apiErrorDTO = new ApiErrorDTO();
        apiErrorDTO.setCode(HttpStatus.NOT_FOUND.value());
        apiErrorDTO.setDescription("Not found element for: " + ex.getMessage());
        return handleExceptionInternal(ex, apiErrorDTO, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}