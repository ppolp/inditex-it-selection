package com.inditex.itselection.application.ports.in.price;

public interface DeletePriceByIdServicePort {
    void execute(Integer id);
}
