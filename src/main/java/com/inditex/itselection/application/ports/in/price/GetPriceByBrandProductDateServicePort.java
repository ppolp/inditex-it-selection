package com.inditex.itselection.application.ports.in.price;

import com.inditex.itselection.application.domain.Price;

import java.time.LocalDateTime;

public interface GetPriceByBrandProductDateServicePort {
    Price execute(Integer brand, Integer productId, LocalDateTime date);
}
