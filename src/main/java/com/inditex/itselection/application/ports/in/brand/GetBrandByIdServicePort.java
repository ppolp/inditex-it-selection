package com.inditex.itselection.application.ports.in.brand;

import com.inditex.itselection.application.domain.Brand;

public interface GetBrandByIdServicePort {
    Brand execute(Integer id);
}
