package com.inditex.itselection.application.ports.out;

import com.inditex.itselection.application.domain.User;

public interface UserRepositoryPort {
    User findByUsername(String username);
}
