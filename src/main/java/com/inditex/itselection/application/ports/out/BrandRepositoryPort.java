package com.inditex.itselection.application.ports.out;

import com.inditex.itselection.application.domain.Brand;

public interface BrandRepositoryPort {
    Brand getById(Integer id);

    Brand save(Brand price);

    void delete(Integer id);
}
