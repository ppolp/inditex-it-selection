package com.inditex.itselection.application.ports.in.price;

import com.inditex.itselection.application.domain.Price;

public interface SaveOrUpdatePriceServicePort {
    Price execute(Integer id, Price price);
}
