package com.inditex.itselection.application.ports.in.brand;

import com.inditex.itselection.application.domain.Brand;

public interface SaveOrUpdateBrandServicePort {
    Brand execute(Integer id, Brand brand);
}
