package com.inditex.itselection.application.ports.out;

import com.inditex.itselection.application.domain.Price;

import java.time.LocalDateTime;

public interface PriceRepositoryPort {
    Price findPriceByBrandAndProductAndDate(Integer brandId, Integer productId, LocalDateTime date);

    Price getById(Integer id);

    Price save(Price price);

    void delete(Integer id);
}
