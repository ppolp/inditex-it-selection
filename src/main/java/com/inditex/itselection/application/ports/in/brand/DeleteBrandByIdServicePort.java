package com.inditex.itselection.application.ports.in.brand;

public interface DeleteBrandByIdServicePort {
    void execute(Integer id);
}
