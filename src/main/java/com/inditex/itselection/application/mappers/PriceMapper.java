package com.inditex.itselection.application.mappers;

import com.inditex.itselection.application.domain.Price;
import com.inditex.itselection.repository.models.PriceMO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PriceMapper {
    PriceMapper INSTANCE = Mappers.getMapper(PriceMapper.class);

    Price fromModel(PriceMO priceMO);

    List<Price> fromModelList(List<PriceMO> priceMOList);

    PriceMO toModel(Price price);

    List<PriceMO> toModelList(List<Price> priceList);
}
