package com.inditex.itselection.application.mappers;

import com.inditex.itselection.application.domain.User;
import com.inditex.itselection.repository.models.UserMO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User fromModel(UserMO userMO);
}
