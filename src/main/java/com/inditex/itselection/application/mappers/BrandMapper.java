package com.inditex.itselection.application.mappers;

import com.inditex.itselection.application.domain.Brand;
import com.inditex.itselection.repository.models.BrandMO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BrandMapper {
    BrandMapper INSTANCE = Mappers.getMapper(BrandMapper.class);

    Brand fromModel(BrandMO brandMO);

    List<Brand> fromModelList(List<BrandMO> brandMOList);

    BrandMO toModel(Brand brand);

    List<BrandMO> toModelList(List<Brand> brandList);
}
