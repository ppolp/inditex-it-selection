package com.inditex.itselection.apirest.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class ApiErrorDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2540925575737388454L;

    private Integer code;

    private String description;
}
