package com.inditex.itselection.apirest.dto;

import lombok.Data;
import org.springframework.data.annotation.ReadOnlyProperty;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class PriceDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = -8938397713912125395L;

    @ReadOnlyProperty
    private Integer id;

    private BrandDTO brand;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Integer priceList;

    private Integer productId;

    private Integer priority;

    private Double price;

    private String currency;
}
