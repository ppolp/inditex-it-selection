package com.inditex.itselection.apirest.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class BrandDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 3941595651150158605L;

    private Integer id;

    private String name;
}
