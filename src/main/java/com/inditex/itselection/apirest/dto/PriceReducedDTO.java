package com.inditex.itselection.apirest.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class PriceReducedDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5883535374684814091L;

    private Integer productId;

    private Integer brandId;

    private Integer priceList;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Double price;
}
