package com.inditex.itselection.apirest.controller;

import com.inditex.itselection.apirest.dto.PriceDTO;
import com.inditex.itselection.apirest.dto.PriceReducedDTO;
import com.inditex.itselection.apirest.mappers.PriceDTOMapper;
import com.inditex.itselection.application.ports.in.price.DeletePriceByIdServicePort;
import com.inditex.itselection.application.ports.in.price.GetPriceByBrandProductDateServicePort;
import com.inditex.itselection.application.ports.in.price.GetPriceByIdServicePort;
import com.inditex.itselection.application.ports.in.price.SaveOrUpdatePriceServicePort;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/prices")
@RequiredArgsConstructor
@Validated
public class PriceController {
    private static final PriceDTOMapper priceDtoMapper = PriceDTOMapper.INSTANCE;

    private final GetPriceByIdServicePort getPriceByIdServicePort;

    private final GetPriceByBrandProductDateServicePort getPriceByBrandProductDateServicePort;

    private final SaveOrUpdatePriceServicePort saveOrUpdatePriceServicePort;

    private final DeletePriceByIdServicePort deletePriceByIdServicePort;

    @GetMapping("/{id}")
    public ResponseEntity<PriceDTO> getPriceById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(priceDtoMapper.toDto(getPriceByIdServicePort.execute(id)));
    }

    @GetMapping
    public ResponseEntity<PriceReducedDTO> getPriceByParams(@RequestParam("date") @DateTimeFormat(iso =
            DateTimeFormat.ISO.DATE_TIME) LocalDateTime date, @RequestParam("productId") Integer productId,
                                                            @RequestParam("brandId") Integer brandId) {
        return ResponseEntity.ok(priceDtoMapper.toReducedDto(getPriceByBrandProductDateServicePort
                .execute(brandId, productId, date)));

    }

    @PostMapping
    public ResponseEntity<PriceDTO> createPrice(@RequestBody @Valid PriceDTO priceDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(priceDtoMapper.toDto(saveOrUpdatePriceServicePort.execute(null,
                        priceDtoMapper.fromDto(priceDto))));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PriceDTO> updatePrice(@PathVariable("id") Integer id, @RequestBody @Valid PriceDTO priceDto) {
        return ResponseEntity.ok(priceDtoMapper.toDto(saveOrUpdatePriceServicePort.execute(id,
                priceDtoMapper.fromDto(priceDto))));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePrice(@PathVariable("id") Integer id) {
        deletePriceByIdServicePort.execute(id);
        return ResponseEntity.noContent().build();
    }
}
