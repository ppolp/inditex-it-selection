package com.inditex.itselection.apirest.controller;

import com.inditex.itselection.apirest.dto.BrandDTO;
import com.inditex.itselection.apirest.mappers.BrandDTOMapper;
import com.inditex.itselection.application.ports.in.brand.DeleteBrandByIdServicePort;
import com.inditex.itselection.application.ports.in.brand.GetBrandByIdServicePort;
import com.inditex.itselection.application.ports.in.brand.SaveOrUpdateBrandServicePort;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/brands")
@RequiredArgsConstructor
@Validated
public class BrandController {
    private static final BrandDTOMapper brandDtoMapper = BrandDTOMapper.INSTANCE;
    private final GetBrandByIdServicePort getBrandByIdServicePort;
    private final SaveOrUpdateBrandServicePort saveOrUpdateBrandServicePort;
    private final DeleteBrandByIdServicePort deleteBrandByIdServicePort;

    @GetMapping("/{id}")
    public ResponseEntity<BrandDTO> getBrandById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(brandDtoMapper.toDto(getBrandByIdServicePort.execute(id)));
    }

    @PostMapping
    public ResponseEntity<BrandDTO> createBrand(@RequestBody @Valid BrandDTO brandDto) {
        return ResponseEntity.ok(brandDtoMapper.toDto(saveOrUpdateBrandServicePort.execute(null,
                brandDtoMapper.fromDto(brandDto))));
    }

    @PutMapping("/{id}")
    public ResponseEntity<BrandDTO> updateBrand(@PathVariable("id") Integer id, @RequestBody @Valid BrandDTO brandDto) {
        return ResponseEntity.ok(brandDtoMapper.toDto(saveOrUpdateBrandServicePort.execute(id,
                brandDtoMapper.fromDto(brandDto))));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBrand(@PathVariable("id") Integer id) {
        deleteBrandByIdServicePort.execute(id);
        return ResponseEntity.noContent().build();
    }
}
