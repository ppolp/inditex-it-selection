package com.inditex.itselection.apirest.mappers;

import com.inditex.itselection.apirest.dto.PriceDTO;
import com.inditex.itselection.apirest.dto.PriceReducedDTO;
import com.inditex.itselection.application.domain.Price;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PriceDTOMapper {

    PriceDTOMapper INSTANCE = Mappers.getMapper(PriceDTOMapper.class);

    PriceDTO toDto(Price price);

    Price fromDto(PriceDTO priceDto);

    @Mapping(source = "brand.id", target = "brandId")
    PriceReducedDTO toReducedDto(Price price);
}
