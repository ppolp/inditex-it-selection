package com.inditex.itselection.apirest.mappers;

import com.inditex.itselection.apirest.dto.BrandDTO;
import com.inditex.itselection.application.domain.Brand;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BrandDTOMapper {
    BrandDTOMapper INSTANCE = Mappers.getMapper(BrandDTOMapper.class);

    BrandDTO toDto(Brand brand);

    Brand fromDto(BrandDTO brandDTO);
}
