package com.inditex.itselection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItselectionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItselectionApplication.class, args);
    }

}
