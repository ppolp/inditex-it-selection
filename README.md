# inditex-it-selection

This is a Spring project that uses H2 in memory database. It is being developed using JDK 17.

## Getting Started

To launch this project you can use the Maven Wrapper script included in the repository.

Here are the steps you can follow:

1. Open a terminal on your operating system.
2. Navigate to the root directory of your project. You can do this with the `cd` command, followed by the path to your
   project directory.
3. Once you are in the directory of your project, you can use the Maven Wrapper script with the `spring-boot:run` goal
   to launch your project. The command would be:

```bash
./mvnw spring-boot:run
```

## API Documentation

This project uses Swagger for API documentation. You can access the Swagger UI at the following URL when the application
is running:

http://localhost:8080/swagger-ui.html